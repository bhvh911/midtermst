﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public abstract class Item : IComparable
    {
        private Guid _id;
        public abstract string Name { get; }
        public abstract double Weight { get; }
        public Guid Id { get { return _id; } }
        public abstract InventorySlotId Slot { get; }
        public abstract bool IsNatural { get; }
        


        public Item()
        {
            _id = Guid.NewGuid();
            //generation of unique id

        }

        public override int GetHashCode()
        {
            // FIXME: return Id.GetHashCode();
            return this.Weight.GetHashCode();
        }
        public int CompareTo(object obj)
        {
            Item items = (Item)obj;
            return this.Id.CompareTo(items.Id);
        }


        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (object.ReferenceEquals(this, obj)) { return true; }
            if (this.GetType() != obj.GetType()) { return false; }

            var othr = (Item)obj;
            return this.Id == othr.Id;
        }

        public override string ToString()
        {

            return string.Format("Name:{0}  IsNatural:{1}  Weight{0}", Name, IsNatural, Weight);
        }
    }
}
