﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class SmallHealthPotion : Item, IPotion
    {

        private int _HealValue;
        private string _name;
        private bool _naturalWeapon;
        private double _weight;
        private InventorySlotId _inventorySlotId;
        private Guid _id;

        public int HealValue { get { return _HealValue; } }
        public override bool IsNatural { get { return _naturalWeapon; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _inventorySlotId; } }


        public SmallHealthPotion() : base()
        {

            _HealValue = 20;
            _naturalWeapon = false;
            _name = "Small Health Potion";
            _weight = 1.00;
            _inventorySlotId = InventorySlotId.POTION1;
            _id = Guid.NewGuid();

        }
        public override string ToString()
        {
            return (base.ToString() + " Attack Value: " + HealValue);
        }

    }
}
