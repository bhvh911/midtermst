﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class GoldChestpeice : Item, IArmor
    {

        private int _DefenseValue;
        private string _name;
        private bool _naturalWeapon;
        private double _weight;
        private InventorySlotId _inventorySlotId;
        private Guid _id;

        public int DefenseValue { get { return _DefenseValue; } }
        public override bool IsNatural { get { return _naturalWeapon; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _inventorySlotId; } }


        public GoldChestpeice() : base()
        {

            _DefenseValue = 5;
            _naturalWeapon = false;
            _name = "Gold Chestpeice";
            _weight = 7.00;
            _inventorySlotId = InventorySlotId.CHESTPIECE;
            _id = Guid.NewGuid();

        }
        public override string ToString()
        {
            return (base.ToString() + " Attack Value: " + DefenseValue);
        }

    }
}
