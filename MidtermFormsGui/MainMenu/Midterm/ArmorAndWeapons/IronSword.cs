﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class IronSword : Item, IWeapon
    {

        private int _attackValue;
        private string _name;
        private bool _naturalWeapon;
        private double _weight;
        private InventorySlotId _inventorySlotId;
        private Guid _id;



        public int AttackValue { get { return _attackValue; } }
        public override bool IsNatural { get { return _naturalWeapon; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _inventorySlotId; } }


        public IronSword(Random random) : base()
        {

            _attackValue = random.Next(5, 10);
            _name = "Odd Iron Sword";
            _naturalWeapon = false;
            _weight = 5.00;
            _inventorySlotId = InventorySlotId.WEAPON;
            _id = Guid.NewGuid();

        }
        public override string ToString()
        {
            return (base.ToString() + " Attack Value: " + AttackValue);
        }

    }
}
