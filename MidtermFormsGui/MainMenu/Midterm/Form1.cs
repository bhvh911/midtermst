﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class Form1 : Form
    {
        private GameManager _gameManager;
        private CombatScreen _combatScreen;
        private GameOver _gameOver;
        private MainMenu _mainMenu;
        private InventoryScreen _inventoryScreen;
        
        public Form1()
        {
            InitializeComponent();
            _gameManager = new GameManager();

            _combatScreen = new CombatScreen(this, _gameManager);
            _combatScreen.Dock = DockStyle.Fill;
            _combatScreen.Visible = false;
            this.Controls.Add(_combatScreen);

            _gameOver = new GameOver(this);
            _gameOver.Dock = DockStyle.Fill;
            _gameOver.Visible = false;
            this.Controls.Add(_gameOver);


            _mainMenu = new MainMenu(this);
            _mainMenu.Dock = DockStyle.Fill;
            _mainMenu.Visible = false;
            this.Controls.Add(_mainMenu);

            _inventoryScreen = new InventoryScreen(this);
            _inventoryScreen.Dock = DockStyle.Fill;
            _inventoryScreen.Visible = false;
            this.Controls.Add(_inventoryScreen);
        }
        public void ShowMainMenu()
        {
            _mainMenu.Visible = true;
            _combatScreen.Visible = false;
            _gameOver.Visible = false;
            _inventoryScreen.Visible = false;
            
        }

        public void ShowCombatScreen()
        {
            _gameManager.NewCreature();
            _combatScreen.Visible = true;
            _gameOver.Visible = false;
            _mainMenu.Visible = false;
            _inventoryScreen.Visible = false;
        }
        public void ShowInventoryScreen()
        {
            _combatScreen.Visible = false;
            _gameOver.Visible = false;
            _mainMenu.Visible = false;
            _inventoryScreen.Visible = true;
        }
        public void ShowGameOver()
        {
            _combatScreen.Visible = false;
            _gameOver.Visible = true;
            _mainMenu.Visible = false;
            _inventoryScreen.Visible = false;
        }


        private void StartButton_Click(object sender, EventArgs e)
        {
            //ShowCombatScreen();
            //var GameStart = new Combat();
            //GameStart.ShowDialog();

            //combatScreen.Visible = true;

        }

        private void combatScreen_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ShowMainMenu();
        }
    }
}
