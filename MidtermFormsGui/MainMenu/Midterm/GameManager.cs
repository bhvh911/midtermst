﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class GameManager
    {
        private Random _random;
        private Random random = new Random();
        private Character _player;
        private Character _enemy;
        private int _depth;
        private bool _gameOver;
   
        

        //------------------------------

        public GameManager()
        {
            //_go = new GameOver(this);

            //_cs = new CombatScreen(this);
            NewCreature();

            _player = new Character("Player", 100, 10);
           
            if (_player.CurrentHealth == 0)
            {
                _gameOver = true;
            }
            if (_gameOver == true)
            {
                GameOver();

                //Goes to end screen
            }

            
            


            if (_enemy.IsDead == true)
            {
                Depth++;
            }

        }

        public void NewCreature()
        {
            int randomNum = random.Next(0, 3);
            switch (randomNum)
            {
                case 0:
                    _enemy = new Character("cat", 25, 10);
                    break;
                case 1:
                    _enemy = new Character("mouse", 20, 5);
                    break;
                case 2:
                    _enemy = new Character("G", 35, 15);
                    break;
                default:
                    _enemy = new Character("cat", 25, 10);
                    break;


            }
        }
        //public void ShowGameOver()
        //{

        //    _gameOver.Visible = true;
        //    _combatScreen.Visible = false;

        //}



        public Character Player
        {
            get
            {
                return _player;
            }
            set
            {
                _player = value;
            }

        }
        public Character Enemy
        {
            get
            {
                return _enemy;
            }
            set
            {
                _enemy = value;
            }

        }
        public int Depth
        {
            get
            {
                return _depth;
            }
            set
            {
                _depth = value;
            }
        }

        public void Attack()
        {
            _player.TakeDamage(_enemy.InitialAttack);
            _enemy.TakeDamage(_player.InitialAttack);

        }
        public void GameOver()
        {
            
        }
    }
}
