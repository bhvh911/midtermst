﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    class ISpell
    {
        int HealValue { get; set; }
        int MaxCharges { get; }
        int ChargesPerUse { get; }
    }
}
