﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class CombatScreen : UserControl
    {
        private GameManager _manager;
        private Form1 _mainMenu;
        private GameOver _gameOver;
        private MainMenu mainMenu;
        private GameManager _gameManager;
        private InventoryScreen _inventoryScreen;

        public CombatScreen(Form1 mainMenu, GameManager gameManager)
        {
            
            _manager = gameManager;
            _mainMenu = mainMenu;
            //_gameOver = gameOver;
            
            InitializeComponent();
            EnemyName.Text = gameManager.Enemy.Name;
            PlayerHealth.Text = "total Health:" + gameManager.Player.CurrentHealth.ToString();
            EnemyHealth.Text = "total Health:" + gameManager.Enemy.CurrentHealth.ToString();
            progressBar1.Value = gameManager.Player.CurrentHealth;
            progressBar2.Value = gameManager.Enemy.CurrentHealth;
        }

        public CombatScreen(MainMenu mainMenu, GameManager gameManager)
        {
            this.mainMenu = mainMenu;
            _gameManager = gameManager;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _manager.Attack();
            progressBar1.Value = _manager.Player.CurrentHealth;
            progressBar2.Value = _manager.Enemy.CurrentHealth;


            SoundPlayer simpleSound = new SoundPlayer(@".\Sound\swoop.wav");
            simpleSound.Play();
            


            if (_manager.Enemy.CurrentHealth <= 0)
            {
                AttackButton.Visible = false;
                button2.Visible = true;
                //insert sound here for death.
            }
            if (_manager.Player.CurrentHealth <= 0)
            {
                _gameOver.Visible = true;
                

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            _mainMenu.ShowInventoryScreen();
            // change to inventory screen
        }

        private void CombatScreen_Load(object sender, EventArgs e)
        {
            
        }

        private void CombatScreen_VisibleChanged(object sender, EventArgs e)
        {
            
        }
    }
}
