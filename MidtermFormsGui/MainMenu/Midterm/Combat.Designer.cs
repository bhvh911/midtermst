﻿namespace Midterm
{
    partial class Combat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Combat));
            this.CharacterBox = new System.Windows.Forms.PictureBox();
            this.EnemyBox = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.CharacterBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnemyBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CharacterBox
            // 
            this.CharacterBox.Image = ((System.Drawing.Image)(resources.GetObject("CharacterBox.Image")));
            this.CharacterBox.Location = new System.Drawing.Point(39, 157);
            this.CharacterBox.Name = "CharacterBox";
            this.CharacterBox.Size = new System.Drawing.Size(201, 252);
            this.CharacterBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.CharacterBox.TabIndex = 0;
            this.CharacterBox.TabStop = false;
            // 
            // EnemyBox
            // 
            this.EnemyBox.Image = ((System.Drawing.Image)(resources.GetObject("EnemyBox.Image")));
            this.EnemyBox.Location = new System.Drawing.Point(528, 157);
            this.EnemyBox.Name = "EnemyBox";
            this.EnemyBox.Size = new System.Drawing.Size(198, 245);
            this.EnemyBox.TabIndex = 1;
            this.EnemyBox.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(313, 223);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 47);
            this.button1.TabIndex = 2;
            this.button1.Text = "Attack";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(313, 446);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 47);
            this.button2.TabIndex = 3;
            this.button2.Text = "Skip Loot";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Location = new System.Drawing.Point(313, 338);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 4;
            this.button3.Text = "Loot";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.ForeColor = System.Drawing.Color.Chartreuse;
            this.progressBar1.Location = new System.Drawing.Point(39, 435);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(186, 24);
            this.progressBar1.TabIndex = 5;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(39, 474);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(186, 147);
            this.listBox1.TabIndex = 6;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(540, 474);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(186, 147);
            this.listBox2.TabIndex = 7;
            // 
            // progressBar2
            // 
            this.progressBar2.ForeColor = System.Drawing.Color.Chartreuse;
            this.progressBar2.Location = new System.Drawing.Point(540, 435);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(186, 24);
            this.progressBar2.TabIndex = 8;
            // 
            // Combat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(833, 633);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.EnemyBox);
            this.Controls.Add(this.CharacterBox);
            this.Name = "Combat";
            this.Text = "Combat";
            ((System.ComponentModel.ISupportInitialize)(this.CharacterBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnemyBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox CharacterBox;
        private System.Windows.Forms.PictureBox EnemyBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ProgressBar progressBar2;
    }
}