﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Character
    {
        protected bool _dead;
        protected StoredItems _bag;
        protected EquippedItems _equipped;
        protected string _name;
       
        protected int _currentHealth;
        protected int _initialAttack;

        public Character(string name, int currenthealth, int initialAttack = 0)
        {
            // FIXME: bag and _equipped not initialized in constructor
            // _bag = new StoredItems(20);
            // _equipped = new EquippedItems();
            
            _name = name;
            _dead = false;
            _currentHealth = currenthealth;
            _initialAttack = initialAttack;
        }


        //-----------------------------------------------------
        public StoredItems Bag
        {
            get { return _bag; }
        }
        public EquippedItems Equipped
        {
            get { return _equipped; }
        }
        public string Name
        {
            get { return _name; }
        }
        public int CurrentHealth
        {
            get { return _currentHealth; }
        }
        public bool IsDead
        {
            get { return _dead; }
        }
        public int InitialAttack
        {
            get
            {
                return _initialAttack;
            }
        }
        //--------------------------------------------------------
        public double CalctotalWeigth()
        {
            return 0;
        }
        public int CalcTotalAttackValue()
        {
            return 10;
        }
        public int CalcTotalDefensevalue()
        {
            return 5;
        }
        public void TakeDamage(int damage)
        {
            //damage = CalcTotalAttackValue() - CalcTotalDefensevalue();
            _currentHealth = _currentHealth -= damage;
            if (_currentHealth <= 0)
            {
                _currentHealth = 0;
                _dead = true;
            }
        }
        public void Pickup(Item item)
        {

        }
        public void UnequipAll()
        {
            
        }

    }
}
