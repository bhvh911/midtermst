﻿namespace Midterm
{
    partial class CombatScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CombatScreen));
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.AttackButton = new System.Windows.Forms.Button();
            this.EnemyBox = new System.Windows.Forms.PictureBox();
            this.CharacterBox = new System.Windows.Forms.PictureBox();
            this.PlayerName = new System.Windows.Forms.Label();
            this.EnemyName = new System.Windows.Forms.Label();
            this.PlayerHealth = new System.Windows.Forms.Label();
            this.EnemyHealth = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EnemyBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CharacterBox)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar2
            // 
            this.progressBar2.ForeColor = System.Drawing.Color.Chartreuse;
            this.progressBar2.Location = new System.Drawing.Point(590, 381);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(186, 24);
            this.progressBar2.TabIndex = 17;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(590, 420);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(186, 147);
            this.listBox2.TabIndex = 16;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(89, 420);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(186, 147);
            this.listBox1.TabIndex = 15;
            // 
            // progressBar1
            // 
            this.progressBar1.ForeColor = System.Drawing.Color.Chartreuse;
            this.progressBar1.Location = new System.Drawing.Point(89, 381);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(186, 24);
            this.progressBar1.TabIndex = 14;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Location = new System.Drawing.Point(363, 276);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 13;
            this.button3.Text = "Loot";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(363, 392);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 47);
            this.button2.TabIndex = 12;
            this.button2.Text = "Inventory";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AttackButton
            // 
            this.AttackButton.Location = new System.Drawing.Point(363, 169);
            this.AttackButton.Name = "AttackButton";
            this.AttackButton.Size = new System.Drawing.Size(124, 47);
            this.AttackButton.TabIndex = 11;
            this.AttackButton.Text = "Attack";
            this.AttackButton.UseVisualStyleBackColor = true;
            this.AttackButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // EnemyBox
            // 
            this.EnemyBox.Image = ((System.Drawing.Image)(resources.GetObject("EnemyBox.Image")));
            this.EnemyBox.Location = new System.Drawing.Point(578, 103);
            this.EnemyBox.Name = "EnemyBox";
            this.EnemyBox.Size = new System.Drawing.Size(198, 245);
            this.EnemyBox.TabIndex = 10;
            this.EnemyBox.TabStop = false;
            // 
            // CharacterBox
            // 
            this.CharacterBox.Image = ((System.Drawing.Image)(resources.GetObject("CharacterBox.Image")));
            this.CharacterBox.Location = new System.Drawing.Point(89, 103);
            this.CharacterBox.Name = "CharacterBox";
            this.CharacterBox.Size = new System.Drawing.Size(201, 252);
            this.CharacterBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.CharacterBox.TabIndex = 9;
            this.CharacterBox.TabStop = false;
            // 
            // PlayerName
            // 
            this.PlayerName.AutoSize = true;
            this.PlayerName.BackColor = System.Drawing.Color.White;
            this.PlayerName.Location = new System.Drawing.Point(163, 64);
            this.PlayerName.Name = "PlayerName";
            this.PlayerName.Size = new System.Drawing.Size(36, 13);
            this.PlayerName.TabIndex = 18;
            this.PlayerName.Text = "Player";
            // 
            // EnemyName
            // 
            this.EnemyName.AutoSize = true;
            this.EnemyName.BackColor = System.Drawing.Color.White;
            this.EnemyName.Location = new System.Drawing.Point(654, 64);
            this.EnemyName.Name = "EnemyName";
            this.EnemyName.Size = new System.Drawing.Size(35, 13);
            this.EnemyName.TabIndex = 19;
            this.EnemyName.Text = "label2";
            // 
            // PlayerHealth
            // 
            this.PlayerHealth.AutoSize = true;
            this.PlayerHealth.BackColor = System.Drawing.Color.White;
            this.PlayerHealth.Location = new System.Drawing.Point(154, 358);
            this.PlayerHealth.Name = "PlayerHealth";
            this.PlayerHealth.Size = new System.Drawing.Size(0, 13);
            this.PlayerHealth.TabIndex = 20;
            // 
            // EnemyHealth
            // 
            this.EnemyHealth.AutoSize = true;
            this.EnemyHealth.BackColor = System.Drawing.Color.White;
            this.EnemyHealth.Location = new System.Drawing.Point(663, 358);
            this.EnemyHealth.Name = "EnemyHealth";
            this.EnemyHealth.Size = new System.Drawing.Size(0, 13);
            this.EnemyHealth.TabIndex = 21;
            // 
            // CombatScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.EnemyHealth);
            this.Controls.Add(this.PlayerHealth);
            this.Controls.Add(this.EnemyName);
            this.Controls.Add(this.PlayerName);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.AttackButton);
            this.Controls.Add(this.EnemyBox);
            this.Controls.Add(this.CharacterBox);
            this.Name = "CombatScreen";
            this.Size = new System.Drawing.Size(865, 670);
            this.Load += new System.EventHandler(this.CombatScreen_Load);
            this.VisibleChanged += new System.EventHandler(this.CombatScreen_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.EnemyBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CharacterBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button AttackButton;
        private System.Windows.Forms.PictureBox EnemyBox;
        private System.Windows.Forms.PictureBox CharacterBox;
        private System.Windows.Forms.Label PlayerName;
        private System.Windows.Forms.Label EnemyName;
        private System.Windows.Forms.Label PlayerHealth;
        private System.Windows.Forms.Label EnemyHealth;
    }
}
